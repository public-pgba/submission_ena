# Author : Maxime BRANGER
# Contact : maxime.branger@inrae.fr
# Date : 2020-01-13
# Update : 2021-03-18
# Version : 2.0
# Object : Create files to preapre submission to assembled genome to ENA/EMBL

import sys
import os
import subprocess
import shutil
import gzip
import argparse
import getpass
import pandas
from Bio import SeqIO

# Checking if USER and PASSWD are provided
def checkLogin(user):
    if user == "":
        USER = input("EBI/EMBL User : ")
    else:
        USER = user
    print("Trying to log as "+USER)
    PASSWD = getpass.getpass("EBI/EMBL password : ")
    return USER, PASSWD

def readMetadata(MetadataFile):
    df_metafile = pandas.read_csv(MetadataFile, sep="\t")
    SAMPLES_INFO = df_metafile.to_dict(orient="records")
    
    return SAMPLES_INFO

def CreateFiles(MetadataFile):
    # STEP 1 : Checking metadata files
    SAMPLES_INFO = readMetadata(MetadataFile)

    print(f"{len(SAMPLES_INFO)} sample(s) to process \n")
    
    for entry in SAMPLES_INFO:
        sample = entry["SAMPLE"]
        
        print(f"Processing {sample} ...")
    
        # STEP 2 : Checking raw files
        if not os.path.isfile(f"Datas/Raw/{entry['FILENAME']}"):
            print(f"\tERROR : {entry['FILENAME']} doesn't exist.")
            ERROR.append(["FILE", sample])
            continue
        else:
            with open(f"Datas/Raw/{entry['FILENAME']}","r") as infile:
                FirstLine = infile.readline()
                if "##gff-version 3" in FirstLine:
                    FORMAT = "GFF"
                elif "LOCUS" in FirstLine:
                    FORMAT = "GENBANK"
                elif ">" in FirstLine:
                    FORMAT = "FASTA"
                elif "ID" in FirstLine:
                    FORMAT = "EMBL"
                else:
                    ERROR.append(["FORMAT", sample])
                    continue

        # STEP 3 : Create manifest file
        if os.path.isfile(f"Manifest/AssemblyGenomes/{sample}.manifest"):
            print("\tManifest already exists. Continue.")
        else:
            with open(f"Manifest/AssemblyGenomes/{sample}.manifest", "w") as OUT:
                OUT.write(f'STUDY\t{entry["PROJECT"]}\n')
                OUT.write(f'SAMPLE\t{entry["SAMPLE"]}\n')
                OUT.write(f'ASSEMBLYNAME\t{entry["ASSEMBLY_NAME"]}\n')
                OUT.write(f'ASSEMBLY_TYPE\t{entry["ASSEMBLY_TYPE"]}\n')
                OUT.write(f'COVERAGE\t{entry["COVERAGE"]}\n')
                OUT.write(f'PROGRAM\t{entry["PROGRAM"]}\n')
                OUT.write(f'PLATFORM\t{entry["PLATFORM"]}\n')
                OUT.write(f'MOLECULETYPE\t{entry["MOLECULETYPE"]}\n')
                if FORMAT == "FASTA":
                    OUT.write(f'FASTA\t{sample}.fasta.gz\n')
                else:
                    OUT.write(f'FLATFILE\t{sample}.embl.gz \n')
        
            print("\tManifest created")
         
        # STEP 4 : Converting GFF to EMBL format
        # IF file is already EMBL, ignore
        if FORMAT == "EMBL":
            if os.path.isfile(f"Datas/Raw/{entry['FILENAME']}"):
                print("\tEMBL already exists. Continue.")
                try:
                    shutil.copyfile(f'Datas/Raw/{entry["FILENAME"]}',f'Datas/Final/{sample}.embl')
                except Exception as e:
                    print(e)
                    continue
            try:
                with open(f'Datas/Final/{sample}.embl', 'rb') as uncompressed:
                    data = uncompressed.read()
                    bindata = bytearray(data)
                    with gzip.open(f'Datas/Final/{sample}.embl.gz', 'wb') as compressed:
                        compressed.write(bindata)

            except Exception as e:
                print(e)
                continue
        elif FORMAT == "GENBANK":
            with open(f'Datas/Raw/{entry["FILENAME"]}', "r") as input_file:
                with open(f'Datas/Final/{sample}.embl', "w") as output_file:
                    datas = SeqIO.convert(input_file, 'genbank', output_file,'embl')
                
                with open(f'Datas/Final/{sample}.embl', 'rb') as uncompressed:
                    data = uncompressed.read()
                    bindata = bytearray(data)
                    with gzip.open(f'Datas/Final/{sample}.embl.gz', 'wb') as compressed:
                        compressed.write(bindata)
        #elif FORMAT == "GFF":
        #    subprocess.call(["gff3_to_embl","--authors",entry["AUTHORS"],"--genome_type",entry["GENOME_TYPE"],"--classification",entry["CLASSIFICATION"],"--output_filename","Datas/Final/"+sample+".embl", entry["TAXONOMY"], entry["TAXID"], entry["PROJECT"], '"strain '+entry["SAMPLE_NAME"]+'"', 'Datas/Raw/'+entry["FILENAME"]])
        #    print("\tEMBL created")
        #    try:
        #        with open(f'Datas/Final/{sample}.embl', 'rb') as uncompressed:
        #            data = uncompressed.read()
        #            bindata = bytearray(data)
        #            with gzip.open(f'Datas/Final/{sample}.embl.gz', 'wb') as compressed:
        #                compressed.write(bindata)

        #    except Exception as e:
        #        print(e)
        #        continue
        ## IF file is FASTA
        else:
            try:
                shutil.copyfile(f'Datas/Raw/{entry["FILENAME"]}',f'Datas/Final/{sample}.fasta')
                with open(f'Datas/Final/{sample}.fasta', 'rb') as uncompressed:
                    data = uncompressed.read()
                    bindata = bytearray(data)
                    with gzip.open(f'Datas/Final/{sample}.fasta.gz', 'wb') as compressed:
                        compressed.write(bindata)

                print("\tFASTA file compressed in GZ.")
            except Exception as e:
                print(e)
                continue

def ProcessingFiles(MetadataFile, USER, action, TestSubmit=None):
    USER, PASSWD = checkLogin(USER) 
    
    # STEP 1 : Checking metadata files
    SAMPLES_INFO = readMetadata(MetadataFile)
    
    print(f"{len(SAMPLES_INFO)} sample(s) to process \n")
    
    for entry in SAMPLES_INFO:
        sample = entry["SAMPLE"]
        print(f"Processing {sample} ...")
    
        # STEP 2 : Checking mandatory files
        
        # Checking manifest
        if not os.path.isfile(f"Manifest/AssemblyGenomes/{sample}.manifest"):
            print("\tERROR - No manifest file")
            ERROR.append(["Nomanifest",sample])
            continue
        else:
            with open(f"Manifest/AssemblyGenomes/{sample}.manifest", "r") as manifile:
                lines = manifile.read().splitlines()
                if 'FASTA' in [line for line in lines]:
                    # Checking FASTA.GZ
                    if not os.path.isfile(f"Datas/Final/{sample}.fasta.gz"):
                        print("\tERROR - No FASTA file")
                        ERROR.append(["NoFASTA",sample])
                        continue

                elif 'FLATFILE' in lines:
                    # Checking EMBL.GZ
                    if not os.path.isfile(f"Datas/Final/{sample}.embl.gz"):
                        print("\tERROR - No EMBL file")
                        ERROR.append(["NoEMBL",sample])
                        continue

                else:
                    ERROR.append(["WrongManifest",sample])

        # STEP 3 : Validating Files
        subprocess.run(f"java -jar bin/webin-cli.jar -userName {USER} -password '{PASSWD}' -context genome -manifest Manifest/AssemblyGenomes/{sample}.manifest -inputDir Datas/Final/ -outputDir Output/{action} -{action}", shell=True)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("FILE", help="Use Metadata/template.tsv as example. Need to be in TAB separate value.")
    parser.add_argument("-a", "--action", required=True, choices=["create", "validate","submit"], help="Some help message")
    parser.add_argument("-u", "--user", help="EMBL/EBI login or email. Required for validate and submit.", default="")
    parser.add_argument("--test", help="Use Test submission service", action="store_true")

    parser.parse_args()
    args = parser.parse_args()

    ACTION = args.action
    INPUT_FILE = args.FILE
    USER = args.user
    PASSWD = ""

    ERROR = []
    
    if args.test:
        TestSubmit = "-test"
    else:
        TestSubmit = ""

    if ACTION == "create":
        CreateFiles(INPUT_FILE)
    elif ACTION == "validate":
        ProcessingFiles(INPUT_FILE, USER, "validate", TestSubmit)
    elif ACTION == "submit":
        ProcessingFiles(INPUT_FILE, USER, "submit", TestSubmit)
    else:
        exit()

    print("\n")
    print(f"{len(ERROR)} error(s) occured") 
    for line in ERROR:
        print(f"Error {line[0]} for sample {line[1]}")
