# Submission ENA pipeline
Author : BRANGER Maxime  
Last Update : 2020-04

## Requirements
 
 * Python 3.8+
 * Java for Webin-CLI
 * Webin-CLI (https://github.com/enasequence/webin-cli/releases)
 	* https://ena-docs.readthedocs.io/en/latest/submit/general-guide/webin-cli.html
 * biopython
 * pandas

 OR (recommended)

 * conda
	conda env create -f environment.yml

## How to use
__Only Illumina Datas can be submit using this script__
### PROJECT and SAMPLES
First, you need a project number and at least one sample number from EBI registration.  
Use this link to register a project and samples : https://www.ebi.ac.uk/ena/submit/   
__How to register a project ?__   
https://ena-docs.readthedocs.io/en/latest/submit/study.html  
__How to register a sample ?__  
https://ena-docs.readthedocs.io/en/latest/submit/samples.html  
### Files to provide
You can provide only 1 type of files :
 * FASTQ file
#### Metadata
Use the template provide in *[Metadata/template_Raw-reads.csv](Metadata/template_Raw-reads.csv)*  
You can use the documentation of Webin-CLI to complete all fields (https://ena-docs.readthedocs.io/en/latest/submit/reads/webin-cli.html)

All fields to complete :
 * PROJECT = Project numbre provided by EBI 
 * SAMPLE = Sample number provided by EBI
 * FILENAME_1 = Name of input file, saved in Datas/Raw/ (Can be FASTQ only)
 * FILENAME_2 = (optionnal) For paired-ends. Name of input file, saved in Datas/Raw/ (Can be FASTQ only)
 * EXPERIMENT = Name of the experiment
 * PLATFORM = Identifier of platform [Informations](https://ena-docs.readthedocs.io/en/latest/submit/reads/webin-cli.html#platform)
 * INSTRUMENT = Identifier of instrument [Informations](https://ena-docs.readthedocs.io/en/latest/submit/reads/webin-cli.html#instrument)
 * LIB_SOURCE = Source of library [Informations](https://ena-docs.readthedocs.io/en/latest/submit/reads/webin-cli.html#source)
 * LIB_SELECT = Method of selection [Informations](https://ena-docs.readthedocs.io/en/latest/submit/reads/webin-cli.html#selection)
 * LIB_START = Strategy of sequencing [Informations](https://ena-docs.readthedocs.io/en/latest/submit/reads/webin-cli.html#strategy)
 * lib_name = (optionnal) Name of the library
 * lib_desc = (optionnal) Description of library
 * insert = (Paired-ends only) Insert size
 
### Use the scripts
If you use conda envrionement, please activate the environment :
    conda activate submission_ena
#### Create files
	
	python Submission_Raw-reads.py -a create Metadata/_yourfile.csv_

#### Validate files

	python Submission_Raw-reads.py -a validate Metadata/_yourfile.csv_

#### Submit files

	python Submission_Assembly-genome.py -a submit Metadata/_yourfile.csv_

