# Submission ENA pipeline
Author : BRANGER Maxime  
Last Update : 2021-04

## How to use
### Submit Raw Reads
Use **Submission_Raw-reads.py**.  
All informations available in [README](README_Raw-reads.md)

### Submit Assembly Genome(s)
Use **Submission_Assembly-genome.py**.  
All informations available in [README](README_Assembly-genome.md)


## FAQ
### Data types
List of data you can submit :
 * Raw Reads
 	* https://ena-docs.readthedocs.io/en/latest/submit/reads.html
 * Assemblies
 	* Isolate Genome Assemblies
		* https://ena-docs.readthedocs.io/en/latest/submit/assembly/genome.html
	* Metagenome Assemblies
		* https://ena-docs.readthedocs.io/en/latest/submit/assembly/metagenome.html
	* Environmental Single-Cell Amplified Genomes
		* https://ena-docs.readthedocs.io/en/latest/submit/assembly/environmental-sag.html
	* Transcriptome Assemblies
		* https://ena-docs.readthedocs.io/en/latest/submit/assembly/transcriptome.html
	* Metatranscriptome Assemblies
		* https://ena-docs.readthedocs.io/en/latest/submit/assembly/metatranscriptome.html
 * Targeted Sequences
 	* Read Alignments
		* https://ena-docs.readthedocs.io/en/latest/submit/analyses/read-alignments.html
	* Sequence Annotations
		* https://ena-docs.readthedocs.io/en/latest/submit/analyses/sequence-annotation.html
	* PacBio Methylation Data
		* https://ena-docs.readthedocs.io/en/latest/submit/analyses/pacbio-methylation.html
	* BioNano Maps
		* https://ena-docs.readthedocs.io/en/latest/submit/analyses/bionano-maps.html
	* Taxonomic Reference Sets
		* https://ena-docs.readthedocs.io/en/latest/submit/analyses/taxonomic-reference-set.html
 * Other Analyses
