# Submission ENA pipeline
Author : BRANGER Maxime  
Last Update : 2020-04

## Requirements
 
 * Python 3.8+
 * Java for Webin-CLI
 * Webin-CLI (https://github.com/enasequence/webin-cli/releases)
 	* https://ena-docs.readthedocs.io/en/latest/submit/general-guide/webin-cli.html
 * biopython
 * pandas

 OR (recommended)

 * conda  
```
	conda env create -f environment.yml
```
## How to use
### PROJECT and SAMPLES
First, you need a project number and at least one sample number from EBI registration.  
Use this link to register a project and samples : https://www.ebi.ac.uk/ena/submit/   
__How to register a project ?__   
https://ena-docs.readthedocs.io/en/latest/submit/study.html  
__How to register a sample ?__  
https://ena-docs.readthedocs.io/en/latest/submit/samples.html  
### Files to provide
You can provide mutliple types of files :
 * FASTA file, without annotations
 * GBK file, containing sequences (full genbank only)
 * GFF file, containing sequences (GFF3 format only)
 * EMBL file, containing annotation
This script will convert GFF or GBK to EMBL before submitting.  
Outputs from Prokka have been tested and validated.  

Input files must be saved in Datas/Raw/ directory.

#### Metadata
Use the template provide in *[Metadata/template_Assembly-genome.tsv](Metadata/template_Assembly-genome.tsv)*  
You can use the documentation of Webin-CLI to complete all fields (https://ena-docs.readthedocs.io/en/latest/submit/assembly/genome.html)

All fields to complete :
 * PROJECT = Project numbre provided by EBI 
 * SAMPLE = Sample number provided by EBI
 * FILENAME = Name of input file, saved in Datas/Raw/ (Can be FASTA, GFF, GBK OR EMBL)
 * ASSEMBLY_NAME = Name of this assembly
 * ASSEMBLY_TYPE = 'clone' or 'isolate'
 * COVERAGE = Your coverage value
 * PROGRAM = Program used to create this assembly
 * PLATFORM = Platform of sequencing (multiple separated by comas)
 * MOLECULETYPE = 'genomic DNA' or 'genomic RNA' or 'viral cRNA'

### Use the scripts
If you use conda envrionement, please activate the environment :
    conda activate submission_ena
#### Create files
	
	python Submission_Assembly-genome.py -a create Metadata/_yourfile.tsv_

#### Validate files

	python Submission_Assembly-genome.py -a validate Metadata/_yourfile.tsv_

#### Submit files

	python Assembly-genome-submission.py -a submit Metadata/_yourfile.tsv_

#### TEST services
Please notice that you can use the TEST interface of EBI by using --test option.
It will validate or submit data registered on the test interface of EBI : https://wwwdev.ebi.ac.uk/ena/submit/sra/
	python Assembly-genome-submission.py --test -a submit Metadata/_yourfile.tsv_
