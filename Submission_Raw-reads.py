# Author : Maxime BRANGER
# Contact : maxime.branger@inrae.fr
# Date : 2020-01-13
# Object : Create files to preapre submission to assembled genome to ENA/EMBL

import sys
import csv
import os
import subprocess
import shutil
import gzip
import argparse
import getpass
import pandas

# Checking if USER and PASSWD are provided
def checkLogin(USER, PASSWD):
    if USER == "":
        USER = getpass.getuser("EBI/EMBL User : ")
    if PASSWD == "":
        print("Trying to log as "+USER)
        PASSWD = getpass.getpass("EBI/EMBL password : ")
    return USER, PASSWD

def readMetadata(MetadataFile):
    df_metafile = pandas.read_csv(MetadataFile, sep="\t")
    SAMPLES_INFO = df_metafile.to_dict(orient="records")
    
    return SAMPLES_INFO

def CreateFiles(MetadataFile):
    # STEP 1 : Checking metadata files
    SAMPLES_INFO = readMetadata(MetadataFile)
    
    print(f"{len(SAMPLES)} sample(s) to process \n")
    
    for entry in SAMPLES_INFO:
        #samp_PRJ = SAMPLES_INFO[sample][0]
        #samp_SMP = SAMPLES_INFO[sample][1]
        #samp_FILE = SAMPLES_INFO[sample][2]
        #samp_FILE2 = SAMPLES_INFO[sample][3]
        #samp_EXP = SAMPLES_INFO[sample][4]
        #samp_PLT = SAMPLES_INFO[sample][5]
        #samp_INS = SAMPLES_INFO[sample][6]
        #samp_LIBsrc = SAMPLES_INFO[sample][7]
        #samp_LIBslt = SAMPLES_INFO[sample][8]
        #samp_LIBstr = SAMPLES_INFO[sample][9]
        #samp_LIBname = SAMPLES_INFO[sample][10]
        #samp_LIBdesc = SAMPLES_INFO[sample][11]
    
        print(f"Processing {sample} ...")
    
        # STEP 2 : Checking raw files
        if not os.path.isfile(f"Datas/Raw/{entry['FILENAME_1']}"):
            print(f"\tERROR : {entry['FILENAME_1']} doesn't exist.")
            ERROR.append(["FILE", sample])
            continue
        else:
            with open(f"Datas/Raw/{entry['FILENAME_1']}","r") as infile:
                FirstLine = infile.readline()
                if "@" in FirstLine:
                    FORMAT = "FASTQ"
                else:
                    print("\tERROR: FORMAT")
                    ERROR.append(["FORMAT", sample])
                    continue
        if samp_FILE2 == "":
            pass
        elif not os.path.isfile(f"Datas/Raw/{entry['FILENAME_2']}"):
            print(f"\tERROR : {entry['FILENAME_2']} doesn't exist.")
            ERROR.append(["FILE2", sample])
            continue
        else:
            with open(f"Datas/Raw/{entry['FILENAME_2']}","r") as infile:
                FirstLine = infile.readline()
                if "@" in FirstLine:
                    FORMAT2 = "FASTQ"
                else:
                    print("\tERROR: FORMAT2")        
                    ERROR.append(["FORMAT2", sample])
                    continue


        # STEP 3 : Create manifest file
        if os.path.isfile("Manifest/RawReads/"+sample+".manifest"):
            print("\tManifest already exists. Continue.")
            try:
                shutil.move('Manifest/RawReads/'+sample+'.manifest','Manifest/RawReads/'+sample+'.manifest')
            except Exception as e:
                print(e)
                continue
        else:
            with open("Manifest/RawReads/"+sample+".manifest", "w") as OUT:
                OUT.write(entry["PROJECT"])
                OUT.write(entry["SAMPLE"])
                OUT.write(entry["EXPERIMENT"])
                if not entry["PLATFORM"] == "":
                    OUT.write(entry["PLATFORM"])
                OUT.write(entry["INSTRUMENT"])
                OUT.write(entry["lib_name"])
                OUT.write(entry["LIB_SOURCE"])
                OUT.write(entry["LIB_SELECT"])
                OUT.write(entry["LIB_STRAT"])
                OUT.write(entry[""])
                OUT.write(entry[""])
                OUT.write(entry[""])
                OUT.write('STUDY\t'+entry["PROJECT"]+'\n')
                OUT.write('SAMPLE\t'+entry["SAMPLE"]+'\n')
                OUT.write('NAME\t'+entry["EXPERIMENT"]+'\n')
                if not entry["PLATFORM"] == "":
                    OUT.write('PLATFORM\t'+entry["PLATFORM"]+'\n')
                OUT.write('INSTRUMENT\t'+entry["INSTRUMENT"]+'\n')
                #OUT.write('INSERT_SIZE\t'+samp_INS+'\n')
                OUT.write('LIBRARY_NAME\t'+entry["lib_name"]+'\n')
                OUT.write('LIBRARY_SOURCE\t'+entry["LIB_SOURCE"]+'\n')
                OUT.write('LIBRARY_SELECTION\t'+entry["LIB_SELECT"]+'\n')
                OUT.write('LIBRARY_STRATEGY\t'+entry["LIB_STRAT"]+'\n')
                if not samp_LIBdesc == "":
                    OUT.write('DESCRIPTION\t'+entry["lib_desc"]+'\n')
                if FORMAT == "FASTQ":
                    OUT.write('FASTQ\t'+sample+'_1.fastq.gz\n')
                else:
                    print("\tERROR: Only FASTQ accepted for this script")
                if FORMAT2 == "FASTQ" and not entry["filename_2"] == "":
                    OUT.write('FASTQ\t'+sample+'_2.fastq.gz\n')
                else:
                    pass
            print("\tManifest created")
         
        # STEP 4 : Compress files
        try:
            shutil.copyfile('Datas/Raw/'+entry["FILENAME_1"],'Datas/Final/'+sample+'_1.fastq')
            with open('Datas/Final/'+sample+'_1.fastq', 'rb') as uncompressed:
                data = uncompressed.read()
                bindata = bytearray(data)
                with gzip.open('Datas/Final/'+sample+'_1.fastq.gz', 'wb') as compressed:
                    compressed.write(bindata)

        except Exception as e:
            print(e)
            continue

        if not samp_FILE2 == "":
            try:
                shutil.copyfile('Datas/Raw/'+entry["FILENAME_2"],'Datas/Final/'+sample+'_2.fastq')
                with open('Datas/Final/'+sample+'_2.fastq', 'rb') as uncompressed:
                    data = uncompressed.read()
                    bindata = bytearray(data)
                    with gzip.open('Datas/Final/'+sample+'_2.fastq.gz', 'wb') as compressed:
                        compressed.write(bindata)
            except Exception as e:
                print(e)
                continue



def ProcessingFiles(MetadataFile, USER, action):
    USER, PASSWD = checkLogin(USER, PASSWD) 
    
    # STEP 1 : Checking metadata files
    SAMPLES_INFO = readMetadata(MetadataFile)
    
    print("%i sample(s) to process \n" % (len(SAMPLES)))
    
    for entry in SAMPLES_INFO:
        sample = entry["SAMPLE"]
        print(f"Processing {sample} ...")
    
        # STEP 2 : Checking mandatory files
        
        # Checking manifest
        if not os.path.isfile("Manifest/RawReads/"+sample+".manifest"):
            print("\tERROR - No manifest file")
            ERROR.append(["Nomanifest",sample])
            continue
        
        ## Checking EMBL.GZ
        #if not os.path.isfile("Datas/Final/"+sample+".fastq.gz"):
        #    print("\tERROR - No EMBL file")
        #    ERROR.append(["NoEMBL",sample])
        #    continue
        
        # STEP 3 : Validating Files
        subprocess.run(['./Webin-CLI_script.sh',
                            action,
                            'Manifest/RawReads/'+sample+'.manifest',
                            USER,
                            PASSWD,
                            'Datas/Final/',
                            'Output/Validate/',
                            'reads'
            ])


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("FILE", help="Use Metadata/template.csv as example.")
    parser.add_argument("-a", "--action", required=True, choices=["create", "validate","submit"], help="Some help message")
    parser.add_argument("-u", "--user", help="EMBL/EBI login. Required for validate and submit.", default="")

    parser.parse_args()
    args = parser.parse_args()

    ACTION = args.action
    INPUT_FILE = args.FILE
    USER = args.user

    ERROR = []

    if ACTION == "create":
        CreateFiles(INPUT_FILE)
    elif ACTION == "validate":
        ProcessingFiles(INPUT_FILE, USER, "VALIDATE")
    elif ACTION == "submit":
        ProcessingFiles(INPUT_FILE, USER, "SUBMIT")
    else:
        exit()


    print("\n")
    print(f"{len(ERROR)} error(s) occured") 
    for line in ERROR:
        print(f"Error {line[0]} for sample {line[1]}")
